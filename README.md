# Título: TEMPLATE PARA GENERAR EL README.md GIT (Sintaxis Markdown)

## Subtitulo (2do nivel)

### Subtitulo (3er nivel)

#### Subtitulo (4to nivel)

Texto normal 

*Texto en cursiva*

**Texto en negrita**

~~Texto tachado~~

***Texto en negrita y cursiva***

- Listado 
- Listado 

1. Numeración
2. Numeración

```
$ cd
$ sudo apt-get update
$ sudo apt-get upgrade
$ exit
```
El anterior es un segmento de código de propósito general (sin resaltado de sintaxis por lenguaje de programación)

```cpp
int a;
int b;
bool done; 

int c = a+b;

if( c > 10)
{
	done = true;
}
else
{
	done = false:
}
```

El anterior es un segmento de código en c++ con resaltado de sintaxis por lenguaje de programación. Las opciones de resaltado más usadas son: c, cpp, python, CMake, MATLAB. Para mayor información o el listado completo del resaltado de sintaxis, remitirse a:

[Github Syntax Highlighting](https://docs.github.com/es/github/writing-on-github/creating-and-highlighting-code-blocks#syntax-highlighting)

[Documentación Markdown](https://markdown.es/sintaxis-markdown/)

Si los anteriores enlaces no funciona, se puede utilizar los siguientes links <https://docs.github.com/es/github/writing-on-github/creating-and-highlighting-code-blocks#syntax-highlighting> y <https://markdown.es/sintaxis-markdown/>

Los cuatro pasos anteriores fueron hipervínculos a una página web

> Esta es una cita

[Ver la imagen](/img/logo-USTA.png)

El anterior es un enlace relativo a un archivo local. Acá se puede utilizar las reglas de navegación de Linux por terminal, como los comando ./ (Dirección del directorio actual) y ../ (Volver al directorio padre/anterior/o superior)

*** 

La anterior fue una regla horizontal (Separación de sección de forma visual por medio de una línea)

![Logo de la Santo Tomás (USTA)](/img/logo-USTA.png)

La anterior es la forma como se agregan imágenes bajo la sintaxis markdown

Si se desea hacer uso de los símbolos reservados y que sean ignorados por el intérprete, se utiliza el auxiliar '\' (barra invertida. Ejemplo

\*\*Esto no va a estar en negrita**

Para hacer una revisión de cómo quedará el texto antes de subirlo a un repositorio git, se hacer uso del siguiente intérprete online: 

[make a readme](https://www.makeareadme.com/)

*** 

Realizado por 
Hernán Josué Hernández Lamprea
hernan.hernandez01@ustabuca.edu.co
Ing. Mecatrónico, Esp. Gerencia de proyectos
Universidad Santo Tomás seccional Bucaramanga, Colombia